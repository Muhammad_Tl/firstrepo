from wagtail.core import blocks
from wagtail.core.blocks import PageChooserBlock, IntegerBlock, CharBlock, DateBlock, URLBlock, RichTextBlock
from wagtail.images.blocks import ImageChooserBlock


class HomePageCarouselImages(blocks.StructBlock):
    """Title and text and nothing else."""
    template = "partial/header.html"

    title = blocks.CharBlock(max_length=100, blank=False, null=True)
    subtitle = blocks.CharBlock(max_length=100, blank=False, null=True)
    next_page = blocks.CharBlock(max_length=100, blank=False, null=True)

    view_more = PageChooserBlock(required=True)

    carousel_image = ImageChooserBlock(required=False)

    class Meta:  # noqa
        template = "partial/header.html"
        icon = "edit"
        label = "HomePageCarouselImages"


class Visions(blocks.StructBlock):
    """Between 1 and 5 images for the home page carousel."""

    text = RichTextBlock(max_length=100, blank=False, null=True)
    explore_more = PageChooserBlock(required=True)

    img1 = ImageChooserBlock(required=False)
    img2 = ImageChooserBlock(required=False)
    img3 = ImageChooserBlock(required=False)
    img4 = ImageChooserBlock(required=False)

    class Meta:  # noqa
        template = "partial/Vision & Mission.html"
        icon = "edit"
        label = "Visions"


class Polls(blocks.StructBlock):
    question = CharBlock(max_length=100, blank=False, null=True)

    class Meta:  # noqa
        template = "partial/polls.html"
        icon = "edit"
        label = "Polls"


class OurService(blocks.StructBlock):
    """Between 1 and 5 images for the home page carousel."""

    image = ImageChooserBlock(required=False)

    description = blocks.RichTextBlock(max_length=200, features=["bold", "italic"], blank=False, null=True)

    lets_start = PageChooserBlock(required=True)

    class Meta:  # noqa
        template = "partial/Start Our Service.html"
        icon = "edit"
        label = "OurService"


class LatestNews(blocks.StructBlock):
    """Between 1 and 5 images for the home page carousel."""

    date = DateBlock(required=True)
    title = CharBlock(max_length=100, blank=False, null=True)
    subtitle = RichTextBlock(max_length=100, blank=False, null=True)
    images = ImageChooserBlock(required=False)
    read_more = PageChooserBlock(required=True)

    view_all = PageChooserBlock(required=True)

    class Meta:  # noqa
        template = "partial/Latest News.html"
        icon = "edit"
        label = "LatestNews"


class Visitors(blocks.StructBlock):
    Unknown = IntegerBlock(required=True)
    Online = IntegerBlock(required=True)
    Total = IntegerBlock(required=True)

    class Meta:  # noqa
        template = "partial/Site visitors.html"
        icon = "edit"
        label = "Visitors"


class FriendlySites(blocks.StructBlock):
    """Between 1 and 5 images for the home page carousel."""

    outLinerLink = URLBlock()
    titled_link = CharBlock(max_length=100, blank=False, null=True)
    imgChooser = ImageChooserBlock(required=False)

    class Meta:  # noqa
        template = "partial/Friendly Sites.html"
        icon = "edit"
        label = "FriendlySites"


# ________________________________________________________________________________
class TitleAndTextBlock(blocks.StructBlock):
    """Title and text and nothing else."""

    title = blocks.CharBlock(required=True, help_text="Add your title")
    text = blocks.TextBlock(required=True, help_text="Add additional text")

    class Meta:  # noqa
        template = "streams/title_and_text_block.html"
        icon = "edit"
        label = "Title & Text"


class CardBlock(blocks.StructBlock):
    """Cards with image and text and button(s)."""

    title = blocks.CharBlock(required=True, help_text="Add your title")

    cards = blocks.ListBlock(
        blocks.StructBlock(
            [
                ("image", ImageChooserBlock(required=True)),
                ("title", blocks.CharBlock(required=True, max_length=40)),
                ("text", blocks.TextBlock(required=True, max_length=200)),
                ("button_page", blocks.PageChooserBlock(required=False)),
                (
                    "button_url",
                    blocks.URLBlock(
                        required=False,
                        help_text="If the button page above is selected, that will be used first.",  # noqa
                    ),
                ),
            ]
        )
    )

    class Meta:  # noqa
        template = "streams/card_block.html"
        icon = "placeholder"
        label = "Staff Cards"


class RichtextBlock(blocks.RichTextBlock):
    """Richtext with all the features."""

    class Meta:  # noqa
        template = "streams/richtext_block.html"
        icon = "doc-full"
        label = "Full RichText"


class SimpleRichtextBlock(blocks.RichTextBlock):
    """Richtext without (limited) all the features."""

    def __init__(
            self, required=True, help_text=None, editor="default", features=None, **kwargs
    ):  # noqa
        super().__init__(**kwargs)
        self.features = ["bold", "italic", "link"]

    class Meta:  # noqa
        template = "streams/richtext_block.html"
        icon = "edit"
        label = "Simple RichText"


class CTABlock(blocks.StructBlock):
    """A simple call to action section."""

    title = blocks.CharBlock(required=True, max_length=60)
    text = blocks.RichTextBlock(required=True, features=["bold", "italic"])
    button_page = blocks.PageChooserBlock(required=False)
    button_url = blocks.URLBlock(required=False)
    button_text = blocks.CharBlock(required=True, default='Learn More', max_length=40)

    class Meta:  # noqa
        template = "streams/cta_block.html"
        icon = "placeholder"
        label = "Call to Action"


class LinkStructValue(blocks.StructValue):
    """Additional logic for our urls."""

    def url(self):
        button_page = self.get('button_page')
        button_url = self.get('button_url')
        if button_page:
            return button_page.url
        elif button_url:
            return button_url

        return None

    # def latest_posts(self):
    #     return BlogDetailPage.objects.live()[:3]


class ButtonBlock(blocks.StructBlock):
    """An external or internal URL."""

    button_page = blocks.PageChooserBlock(required=False, help_text='If selected, this url will be used first')
    button_url = blocks.URLBlock(required=False,
                                 help_text='If added, this url will be used secondarily to the button page')

    # def get_context(self, request, *args, **kwargs):
    #     context = super().get_context(request, *args, **kwargs)
    #     context['latest_posts'] = BlogDetailPage.objects.live().public()[:3]
    #     return context

    class Meta:  # noqa
        template = "streams/button_block.html"
        icon = "placeholder"
        label = "Single Button"
        value_class = LinkStructValue
