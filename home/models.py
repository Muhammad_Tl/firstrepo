from django.db import models
from django.shortcuts import render

from modelcluster.fields import ParentalKey

from wagtail.admin.edit_handlers import (
    FieldPanel,
    MultiFieldPanel,
    InlinePanel,
    StreamFieldPanel,
    PageChooserPanel,
)
from django.db import models
from wagtail.core.models import Page, Orderable
from wagtail.core.fields import RichTextField, StreamField
from wagtail.images.edit_handlers import ImageChooserPanel
from wagtail.contrib.routable_page.models import RoutablePageMixin, route
from streams import blocks


class HomePage(RoutablePageMixin, Page):
    """Home page model."""

    template = "home/home_page.html"
    max_count = 1
    facebook_link = models.URLField()
    twitter_link = models.URLField()
    instagram_link = models.URLField()

    banner = StreamField([("HomePageCarouselImages", blocks.HomePageCarouselImages())], null=True, blank=True)
    Visions = StreamField([("Visions", blocks.Visions())], null=True, blank=True)
    Polls = StreamField([("Polls", blocks.Polls())], null=True, blank=True)
    OurService = StreamField([("OurService", blocks.OurService())], null=True, blank=True)
    LatestNews = StreamField([("LatestNews", blocks.LatestNews())], null=True, blank=True)
    Visitors = StreamField([("Visitors", blocks.Visitors())], null=True, blank=True)
    FriendlySites = StreamField([("FriendlySites", blocks.FriendlySites())], null=True, blank=True)

    content_panels = Page.content_panels + [
        FieldPanel('facebook_link'),
        FieldPanel('twitter_link'),
        FieldPanel('instagram_link'),
        StreamFieldPanel("banner"),
        StreamFieldPanel("Visions"),
        StreamFieldPanel("Polls"),
        StreamFieldPanel("OurService"),
        StreamFieldPanel("LatestNews"),
        StreamFieldPanel("Visitors"),
        StreamFieldPanel("FriendlySites")
    ]

    class Meta:
        verbose_name = "Home Page"
        verbose_name_plural = "Home Pages"

    # def get_context(self, request, *args, **kwargs):
    #     context = super().get_context(request, *args, **kwargs)
    #     context["banner_data"] = list(
    #         map(
    #             lambda element: dict(element.value),
    #             context["self"].content
    #         )
    #     )
    #     return context
