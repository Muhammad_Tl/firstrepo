# Generated by Django 3.2.7 on 2021-09-16 10:16

from django.db import migrations, models
import django.db.models.deletion
import modelcluster.fields
import wagtail.core.fields


class Migration(migrations.Migration):

    dependencies = [
        ('wagtailcore', '0062_comment_models_and_pagesubscription'),
        ('wagtailimages', '0023_add_choose_permissions'),
        ('home', '0009_auto_20210915_1324'),
    ]

    operations = [
        migrations.RenameField(
            model_name='homepagecarouselimages',
            old_name='header',
            new_name='next',
        ),
        migrations.RenameField(
            model_name='latestnews',
            old_name='imgChooser',
            new_name='images',
        ),
        migrations.RenameField(
            model_name='latestnews',
            old_name='header',
            new_name='title',
        ),
        migrations.RenameField(
            model_name='ourservice',
            old_name='imgChooser',
            new_name='image',
        ),
        migrations.RenameField(
            model_name='ourservice',
            old_name='read_more',
            new_name='lets_start',
        ),
        migrations.RenameField(
            model_name='visions',
            old_name='read_more',
            new_name='explore_more',
        ),
        migrations.RenameField(
            model_name='visions',
            old_name='imgChooser',
            new_name='img1',
        ),
        migrations.RemoveField(
            model_name='visions',
            name='description',
        ),
        migrations.AddField(
            model_name='homepage',
            name='facebook_link',
            field=models.URLField(default=1),
            preserve_default=False,
        ),
        migrations.AddField(
            model_name='homepage',
            name='instagram_link',
            field=models.URLField(default=1),
            preserve_default=False,
        ),
        migrations.AddField(
            model_name='homepage',
            name='twitter_link',
            field=models.URLField(default=1),
            preserve_default=False,
        ),
        migrations.AddField(
            model_name='homepagecarouselimages',
            name='title',
            field=models.CharField(max_length=100, null=True),
        ),
        migrations.AddField(
            model_name='homepagecarouselimages',
            name='view_more',
            field=models.ForeignKey(blank=True, null=True, on_delete=django.db.models.deletion.SET_NULL, related_name='+', to='wagtailcore.page'),
        ),
        migrations.AddField(
            model_name='latestnews',
            name='view_all',
            field=models.ForeignKey(blank=True, null=True, on_delete=django.db.models.deletion.SET_NULL, related_name='+', to='wagtailcore.page'),
        ),
        migrations.AddField(
            model_name='visions',
            name='img2',
            field=models.ForeignKey(null=True, on_delete=django.db.models.deletion.SET_NULL, related_name='+', to='wagtailimages.image'),
        ),
        migrations.AddField(
            model_name='visions',
            name='img3',
            field=models.ForeignKey(null=True, on_delete=django.db.models.deletion.SET_NULL, related_name='+', to='wagtailimages.image'),
        ),
        migrations.AddField(
            model_name='visions',
            name='img4',
            field=models.ForeignKey(null=True, on_delete=django.db.models.deletion.SET_NULL, related_name='+', to='wagtailimages.image'),
        ),
        migrations.AddField(
            model_name='visions',
            name='text',
            field=wagtail.core.fields.RichTextField(max_length=100, null=True),
        ),
        migrations.AlterField(
            model_name='latestnews',
            name='subtitle',
            field=wagtail.core.fields.RichTextField(max_length=100, null=True),
        ),
        migrations.AlterField(
            model_name='ourservice',
            name='description',
            field=wagtail.core.fields.RichTextField(max_length=200, null=True),
        ),
        migrations.CreateModel(
            name='Polls',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('qustion', models.CharField(max_length=100, null=True)),
                ('page', modelcluster.fields.ParentalKey(on_delete=django.db.models.deletion.CASCADE, related_name='polls', to='home.homepage')),
            ],
        ),
    ]
