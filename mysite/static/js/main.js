$(function () {
    "use strict";

    $('.loading-screen').delay(1400).fadeOut();
    /*Add affix class to navbar on scroll*/
    $(window).scroll(function() {
        /* affix after scrolling 100px */
        if ($(document).scrollTop() > 100) {
            $('.navbar').addClass('affix');
        } else {
            $('.navbar').removeClass('affix');
        }
    });

    $('.press-center-li').click(function(){
        $('.sub-menu').slideToggle();
        $(this).toggleClass('active');
    });

    /*Search*/
    $('#search-icon').click(function(e){
        e.preventDefault();
        $('.search-box .search-input').toggleClass('active');
        $('.search-toggle span').toggleClass('active');
    });

    /*Open Menu*/
    $('#openMenu').click(function(){
        $('.overlay-menu').addClass('active-menu');
    });

    /*Close Menu*/
    $('#close_icon').click(function(){
        $('.overlay-menu').removeClass('active-menu');
    });

    /*Start Header Slider*/
    $('.header-slider').on('init reInit afterChange', function(event, slick, currentSlide, nextSlide){
        var i = (currentSlide ? currentSlide : 0) + 1;
        if(i<10){
            var num = '0' + i;
            var count = '0'
        }
        if(slick.slideCount<10){
            var count = '0' + slick.slideCount;
        }
        $('.slick-slider-dots').html(num + '<span class="line"></span>' + count);
        $('.slick-current .slide-info').addClass('active');
        
        if($('.header-slider .item').hasClass('slick-current')){
            $('.view-more').removeClass('border-animate');
            $('.view-more').delay(3000).addClass('border-animate');
        }
    });

    if (window.matchMedia("(min-width: 768px)").matches) {
        $(".header-slider").on("beforeChange", function(event, slick, currentSlide, nextSlide) {
            $(".header-slider .slick-list").addClass("do-tans");
        })
        .on("afterChange", function() {
            $(".header-slider .slick-list").removeClass("do-tans");
        })
    }

    $('.header-slider').slick({
        rtl: $("html").attr("lang") === "ar",
        dots: false,
        arrows:true,
        infinite: true,
        // fade: true,
        // speed: 500,
        cssEase: 'linear',
        slidesToShow: 1,
        slidesToScroll: 1,
        autoplay: true,
        autoplaySpeed: 2000,
        prevArrow: $("#prev-slide"),
        nextArrow: $("#next-slide"),
    });
    /*End Header Slider*/


    /*Start Services Slider*/
    $("#wheel").flipster({
        style: 'wheel',
        spacing: 0.1,
        start: 'center',
        itemContainer: 'ul',
        itemSelector: 'li',
        scrollwheel: true,
        buttons: 'custom',
        // autoplay: 2000,
        // enableMousewheel: false,
        scrollwheel: false,
        fadeIn: 400,
        loop: true,
        buttonPrev: '<i class="fa fa-long-arrow-left" aria-hidden="true"></i>',
        buttonNext: '<i class="fa fa-long-arrow-right" aria-hidden="true"></i>',
        onItemSwitch: function(){
            // if($('.video-src').parent().parent().hasClass('flipster__item--past-1')){
            //     $('.video-src').get(0).play();
            // }else{
            //     $('.video-src').get(0).pause();
            // }
            if($('.video-src').parent().parent().hasClass('flipster__item--past-1')){
                $('.flipster__item--past-1 .video-src').trigger('play');
            }else if($('.video-src').parent().parent().hasClass('flipster__item--future-1')){
                $('.flipster__item--future-1 .video-src').trigger('play');
            }
            // $('.flipster__item--current').prev().$('.video-src').trigger('play');
        }
    });
    /*End Services Slider*/

    /*Start Latest News Slider*/
    $('.lates-news-slider').on('init reInit afterChange', function(event, slick, currentSlide, nextSlide){
        var i = (currentSlide ? currentSlide : 0) + 1;
        if(i<10){
            var num = '0' + i;
            var count = '0'
        }
        if(slick.slideCount<10){
            var count = '0' + slick.slideCount;
        }
        $('.slick-slider-dots1').html('<span class="first">' + num + '</span>'+ '/' + '<span class="count">' + count + '</span>');
    });

    $('.lates-news-slider').slick({
        rtl: $("html").attr("lang") === "ar",
        dots: false,
        arrows:true,
        infinite: true,
        slidesToShow: 1,
        slidesToScroll: 1,
        fade: true,
        speed: 500,
        cssEase: 'linear',
        autoplay: true,
        autoplaySpeed: 2000,
        asNavFor: '.lates-news-slider-nav'
    });
    $('.lates-news-slider-nav').slick({
        // rtl: $("html").attr("lang") === "ar",
        dots: false,
        arrows:true,
        infinite: true,
        centerMode: true,
        centerPadding: "200px",
        slidesToShow: 1,
        slidesToScroll: 1,
        autoplay: true,
        autoplaySpeed: 2000,
        vertical: true,
        verticalSwiping:true,
        prevArrow: $("#prev-news"),
        nextArrow: $("#next-news"),
        asNavFor: '.lates-news-slider',
        responsive: [{
            breakpoint: 992,
            settings: {
                centerPadding: "0",
            }
        },
        {
            breakpoint: 1199,
            settings: {
                centerPadding: "150px",
            }
        }]
    });
    /*End Latest News Slider*/

    /*Start Friendly Sites Slider*/
    $('.friendly-sites-slider').slick({
        rtl: $("html").attr("lang") === "ar",
        dots: false,
        arrows:true,
        infinite: true,
        centerMode: true,
        slidesToShow: 3,
        slidesToScroll: 1,
        autoplay: true,
        autoplaySpeed: 2000,
        prevArrow: $("#prev-site"),
        nextArrow: $("#next-site"),
        responsive: [{
            breakpoint: 992,
            settings: {
                slidesToShow: 2,
                centerPadding: "100px",
            }
        },
        {
            breakpoint: 768,
            settings: {
                slidesToShow: 2,
                centerPadding: "50px",
            }
        },
        {
            breakpoint: 576,
            settings: {
                slidesToShow: 1,
                centerPadding: "50px",
            }
        }]
    });
    /*End Friendly Sites Slider*/

    function animateBar(yes, no){
        $('#yes').width(yes+'%');
        $('#no').width(no+'%');
    }

    /*vote*/
    $('#vote').click(function () {
        $(this).fadeOut();
        $('.polls-section .content p').fadeOut();
        $('.polls-section .content .radio-btns').fadeOut();
        setTimeout( function(){
            // $('.polls-section .content').css('width', '43%');
            $('.polls-section .content .progress-section').css('display', 'flex');
            animateBar(85, 30);
        }, 500);
    });

    if (window.matchMedia("(min-width: 1400px)").matches) {
        $('#vote').click(function () {
            setTimeout( function(){
                $('.polls-section .content').css('width', '43%');
            }, 500);
        });
    } else if (window.matchMedia("(min-width: 1200px)").matches) {
        $('#vote').click(function () {
            setTimeout( function(){
                $('.polls-section .content').css('width', '55%');
            }, 500);
        });
    }else if (window.matchMedia("(min-width: 768px)").matches) {
        $('#vote').click(function () {
            setTimeout( function(){
                $('.polls-section .content').css('width', '63%');
            }, 500);
        });
    }else{
        $('#vote').click(function () {
            setTimeout( function(){
                $('.polls-section .content').css('width', '100%');
            }, 500);
        });
    }

    /*Animation*/
    AOS.init({
        disable: function() {
            var maxWidth = 992;
            return window.innerWidth < maxWidth;
        }
    });
    
    /*Start News & Events Page*/

    var $slider = $('.current-events-slider');
    var $progressBar1 = $('.progress');
    
    $slider.on('beforeChange', function(event, slick, currentSlide, nextSlide) {   
        var oo = 100 / slick.slideCount;
        $progressBar1
          .css("background-size", (nextSlide + 1) * oo + "% 100%");
    });

    $('.current-events-slider').slick({
        rtl: $("html").attr("lang") === "ar",
        dots: false,
        arrows:false,
        infinite: true,
        speed: 300,
        slidesToShow: 4,
        slidesToScroll: 1,
        autoplay: true,
        autoplaySpeed: 2000,
        responsive: [
            {
                breakpoint: 992,
                settings: {
                    slidesToShow: 3,
                    slidesToScroll: 1
                }
            },
            {
                breakpoint: 576,
                settings: {
                    slidesToShow: 2,
                    slidesToScroll: 1
                }
            },
            {
                breakpoint: 420,
                settings: {
                    slidesToShow: 1,
                    slidesToScroll: 1
                }
            }
        ]
    });

    $('.upcoming-events-slider').slick({
        rtl: $("html").attr("lang") === "ar",
        dots: false,
        arrows:false,
        infinite: true,
        speed: 300,
        slidesToShow: 3,
        slidesToScroll: 1,
        autoplay: true,
        autoplaySpeed: 2000,
        responsive: [
            {
                breakpoint: 576,
                settings: {
                    slidesToShow: 2,
                    slidesToScroll: 1
                }
            },
            {
                breakpoint: 420,
                settings: {
                    slidesToShow: 1,
                    slidesToScroll: 1
                }
            }
        ]
    });

    $('.previous-events-slider').slick({
        rtl: $("html").attr("lang") === "ar",
        dots: false,
        arrows:false,
        infinite: true,
        speed: 300,
        slidesToShow: 4,
        slidesToScroll: 1,
        autoplay: true,
        autoplaySpeed: 2000,
        responsive: [
            {
                breakpoint: 992,
                settings: {
                    slidesToShow: 3,
                    slidesToScroll: 1
                }
            },
            {
                breakpoint: 576,
                settings: {
                    slidesToShow: 2,
                    slidesToScroll: 1
                }
            },
            {
                breakpoint: 420,
                settings: {
                    slidesToShow: 1,
                    slidesToScroll: 1
                }
            }
        ]
    });
    /*End News & Events Page*/

    /*Start News Details Page*/
    $('.news-details-slider').slick({
        rtl: $("html").attr("lang") === "ar",
        dots: true,
        arrows:false,
        infinite: true,
        speed: 300,
        slidesToShow: 1,
        slidesToScroll: 1,
        autoplay: true,
        autoplaySpeed: 2000,
    });
    /*End News Details Page*/
    $(".buttons-group .filter-btn").click(function () {
        $(this).addClass("active").siblings().removeClass("active");
    });
});
